
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import saveIcon from '../svg/save.svg';

// /////////
// Save button plugin
export default class SfeSave extends Plugin {
	init() {
		const editor = this.editor;
		
		editor.ui.componentFactory.add( 'sfeSave', locale => {
			const view = new ButtonView( locale );
			// view.isEnabled = false;
			
			view.set( {
				label: 'Save this content',
				icon: saveIcon,
				tooltip: true
			} );
			
			// Callback executed once the image is clicked.
			view.on( 'execute', () => {
				editor.sfe.save(editor);
			} );
			
			return view;
		} );
	}
}
