
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import cancelIcon from '@ckeditor/ckeditor5-core/theme/icons/cancel.svg';

// /////////
// Cancel button plugin
export default class SfeCancel extends Plugin {
	init() {
		const editor = this.editor;
		
		editor.ui.componentFactory.add( 'sfeCancel', locale => {
			const view = new ButtonView( locale );
			
			view.set( {
				label: 'Cancel edition',
				icon: cancelIcon,
				tooltip: true
			} );
			
			// Callback executed once the image is clicked.
			view.on( 'execute', () => {
				editor.sfe.cancel(editor);
			} );
			
			return view;
		} );
	}
}
