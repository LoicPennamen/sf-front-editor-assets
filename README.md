# Symfony Front Editor #

In-page content editor for Symfony 4+ applications. This package aims to provide 
user with a fast RTE editor for HTML blocks.

This package handles the client-side interactions. For the back-end package, go to:  
https://packagist.org/packages/loicpennamen/sf-front-editor

This is a work in progress.

### TODO ###

* Lors de l'annulation, récupérer "vraiment" le contenu d'origine non formaté par ckeditor.
* Gérer le Ctrl-S
* Chargement de versions précédentes
* Confirm lors du quittage de page si édition en cours

### Installation ###

* Please refer to the README file in the server-side package:  
https://bitbucket.org/LoicPennamen/sf-front-editor/src/master/README.md

### Development ###

Watch & run:  
``webpack -w``  
ou  
``node_modules/.bin/webpack --watch``

Deploy to NPM: Change version in ``package.json`` then:  
``npm publish``

### Who do I talk to? ###

Loïc Pennamen  
http://loicpennamen.com
